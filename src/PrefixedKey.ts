export class PrefixedKey {
	public rawKey: string;
	public prefix?: string;
	public key!: string;

	constructor(key: string) {
		this.rawKey = key;
		this.decomposeKey(key);
	}

	public get isPrefixed(): boolean {
		return !!this.prefix;
	}

	public get isUnprefixed(): boolean {
		return !this.isPrefixed;
	}

	private decomposeKey(key: string): void {
		let parts = key.split(".");

		if (parts.length == 1) {
			this.key = parts[0];
		}
		else if (parts.length == 2) {
			this.prefix = parts[0];
			this.key = parts[1];
		}
		else {
			throw new Error(`Invalid key: '${key}'. Multiple prefixes are not supported.`);
		}
	}
}