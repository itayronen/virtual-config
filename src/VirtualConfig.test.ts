import { TestSuite } from "just-test-api";
import { expect } from "chai";
import { ConfigCreator } from "./VirtualConfig";

export default function (suite: TestSuite) {
	suite.describe("createByOrefix", suite => {
		suite.describe("unprefixed keys", suite => {

			suite.test("number.", () => {
				let creator = new ConfigCreator();
				let config = creator.createByPrefix({ inputConfig: { hello: 5 } });
				expect(config.hello).to.equal(5);
			});

			suite.test("boolean.", () => {
				let creator = new ConfigCreator();
				let config = creator.createByPrefix({ inputConfig: { hello: true } });
				expect(config.hello).to.equal(true);
			});

			suite.test("string.", () => {
				let creator = new ConfigCreator();
				let config = creator.createByPrefix({ inputConfig: { hello: "world" } });
				expect(config.hello).to.equal("world");
			});

			suite.test("null.", () => {
				let creator = new ConfigCreator();
				let config = creator.createByPrefix({ inputConfig: { hello: null } });
				expect(config.hello).to.equal(null);
			});

			suite.test("undefined.", () => {
				let creator = new ConfigCreator();
				let config = creator.createByPrefix({ inputConfig: { hello: undefined } });
				expect(config.hello).to.equal(undefined);
			});

			suite.test("object.", () => {
				let creator = new ConfigCreator();
				let config = creator.createByPrefix({ inputConfig: { hello: { there: "world" } } });
				expect(config.hello.there).to.equal("world");
			});

			suite.test("array.", () => {
				let creator = new ConfigCreator();
				let config = creator.createByPrefix({ inputConfig: { hello: [{ there: "world" }, { bla: "bla" }] } });
				expect(config.hello[0].there).to.equal("world");
				expect(config.hello[1].bla).to.equal("bla");
			});

			suite.test("prefixed is ignored.", () => {
				let creator = new ConfigCreator();
				let config = creator.createByPrefix({ inputConfig: { "a.hello": 5 } });
				expect(config.hello).to.equal(undefined);
			});

		});

		suite.describe("prefixed config", suite => {

			suite.test("number - unprefixed.", () => {
				let creator = new ConfigCreator();
				let config = creator.createByPrefix({ prefix: "a", inputConfig: { "hello": 5 } });
				expect(config.hello).to.equal(5);
			});

			suite.test("prefixed number.", () => {
				let creator = new ConfigCreator();
				let config = creator.createByPrefix({ prefix: "a", inputConfig: { "a.hello": 5 } });
				expect(config.hello).to.equal(5);
				expect(config["a.hello"]).to.equal(undefined);
			});

			suite.test("prefixed number - wrong prefix.", () => {
				let creator = new ConfigCreator();
				let config = creator.createByPrefix({ prefix: "a", inputConfig: { "b.hello": 5 } });
				expect(config.hello).to.equal(undefined);
				expect(config["a.hello"]).to.equal(undefined);
			});

			suite.test("prefixed key overrides unprefixed key.", test => {
				let creator = new ConfigCreator();
				let config = creator.createByPrefix({ prefix: "a", inputConfig: { hello: 3, "a.hello": 5 } });
				expect(config.hello).to.equal(5);
			});

			suite.test("prefixed object.", () => {
				let creator = new ConfigCreator();
				let config = creator.createByPrefix({ prefix: "a", inputConfig: { "a.hello": { there: "world" } } });
				expect(config.hello.there).to.equal("world");
			});

			suite.test("prefixed inner object.", () => {
				let creator = new ConfigCreator();
				let config = creator.createByPrefix({
					prefix: "a", inputConfig: {
						hello: {
							"b.there": "bad",
							"a.there": "world",
							"c.there": "bad"
						}
					}
				});
				expect(config.hello.there).to.equal("world");
			});

			suite.test("prefixed array.", test => {
				let creator = new ConfigCreator();
				let config = creator.createByPrefix({ prefix: "a", inputConfig: { "a.hello": [{ there: "world" }, { "a.my": "hey" }] } });
				expect(config.hello[0].there).to.equal("world");
				expect(config.hello[1].my).to.equal("hey");
			});

			suite.test("Multiple prefixes should throw.", test => {
				let creator = new ConfigCreator();
				expect(() => creator.createByPrefix({ inputConfig: { "a.b.hello": 5 } })).to.throw();
			});
		});

		suite.describe("README examples", suite => {
			suite.test("Example1.", () => {
				let inputConfig = {
					"dev.server": "localhost:3000",
					"prod.server": "production.com",
					"test.server": "testing.com",

					timeout: 5000,
					"dev.timeout": 9000,

					someObject: {
						hello: "default world",
						"prod.hello": "production world"
					},

					someArray: [{
						monstersAndMen: "good music",
						"prod.status": "live",
						"dev.status": "17/1/2015"
					}, {
						theLonelyIsland: "funny music",
						"prod.status": "live",
						"dev.status": "10/10/2014"
					}]
				};

				let creator = new ConfigCreator();
				let configDev = creator.createByPrefix({ prefix: "dev", inputConfig: inputConfig });
				let configProd = creator.createByPrefix({ prefix: "prod", inputConfig: inputConfig });

				//  console.log(configDev);
				//  console.log(configProd);
			});
		});
	});
}
