import { PrefixedKey } from "./PrefixedKey";

export interface CreateByPrefixParams {
	inputConfig: any;
	prefix?: string;
}

export class ConfigCreator {
	constructor() {
	}

	public createByPrefix(params: CreateByPrefixParams): any {
		let config: any = this.filterObjectByPrefix(params);

		return config;
	}

	private filterObjectByPrefix(params: CreateByPrefixParams): any {
		let filterdObject: any = {};

		for (let rawKey in params.inputConfig) {
			let prefixedKey = new PrefixedKey(rawKey);
			let keyValue: any = params.inputConfig[prefixedKey.rawKey];

			if (prefixedKey.isUnprefixed) {
				filterdObject[prefixedKey.key] = this.getPrefixedValue(keyValue, params);
			}
			else if (params.prefix && prefixedKey.prefix === params.prefix) {
				filterdObject[prefixedKey.key] = this.getPrefixedValue(keyValue, params);
			}
		}

		return filterdObject;
	}

	private getPrefixedValue(value: any, params: CreateByPrefixParams): object {
		if (this.isPrimitive(value)) {
			return value;
		}
		else if (typeof value == "object") {
			if (this.isObject(value)) {
				return this.filterObjectByPrefix(this.clonePrefixParams(params, value));
			}
			else if (Array.isArray(value)) {
				return (<object[]>value).map(arrayValue => this.filterObjectByPrefix(this.clonePrefixParams(params, arrayValue)));
			}
			else {
				throw new Error(`Value cannot be instance of an unapproved class: '${value.constructor.name}'.`);
			}
		}
		else {
			throw new Error(`Unsupported value type '${typeof value}'.`);
		}
	}

	private clonePrefixParams(params: CreateByPrefixParams, newInput?: any): CreateByPrefixParams {
		return {
			inputConfig: newInput || params.inputConfig,
			prefix: params.prefix,
		};
	}

	private isPrimitive(value: any): boolean {
		return typeof value == "string" ||
			typeof value == "number" ||
			typeof value == "boolean" ||
			typeof value == "undefined" || value == null
	}

	private isObject(value: any): boolean {
		return value.constructor.name == "Object"
	}
}