# virtual-config
Config solution for multi environment applications, written in typescript.

## Install
npm install --save virtual-config

## Usage
```ts
import { ConfigCreator } from "virtual-config";

let inputConfig = {
	"dev.server": "localhost:3000",
	"prod.server": "production.com",
	"test.server": "testing.com",

	timeout: 5000,
	"dev.timeout": 9000,

	someObject: {
		hello: "default world",
		"prod.hello": "production world"
	},

	someArray: [
		{
			monstersAndMen: "good music",
			"prod.status": "live",
			"dev.status": "17/1/2015"
		},
		{
			theLonelyIsland: "funny music",
			"prod.status": "live",
			"dev.status": "10/10/2014"
		}
	]
};

let creator = new ConfigCreator();
let configDev = creator.createByPrefix({ prefix: "dev", inputConfig: inputConfig });
let configProd = creator.createByPrefix({ prefix: "prod", inputConfig: inputConfig });

```
Dev config value:
```
{
	server: 'localhost:3000',
	timeout: 9000,
	someObject: { hello: 'default world' },
	someArray: [
		{ monstersAndMen: 'good music', status: '17/1/2015' },
		{ theLonelyIsland: 'funny music', status: '10/10/2014' }
	]
}
```
Prod config value:
```
{
	server: 'production.com',
	timeout: 5000,
	someObject: { hello: 'production world' },
	someArray: [
		{ monstersAndMen: 'good music', status: 'live' },
		{ theLonelyIsland: 'funny music', status: 'live' }
	]
}
```
